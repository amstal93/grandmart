import React from "react";
import ReactNotification from "react-notifications-component";
import Head from "./Head";

function App({ children }) {
  return (
    <>
      <Head />
      <ReactNotification />
      {children}
    </>
  );
}

export default App;
