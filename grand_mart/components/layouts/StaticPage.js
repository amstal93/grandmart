import React from "react";
import App from "./App";
import Header from "../Header";
import Footer from "../Footer";
import Features from "../Features";
import CartSidebar from "../CartSidebar";
import MenusSidebar from "../MenusSidebar";
import NavigationSidebar from "../static-page/NavigationSidebar";
import Breadcrumb from "../static-page/Breadcrumb";
import PropTypes from "prop-types";

const StaticPage = ({ children, title, hasNavigation, hasTitle }) => (
  <App>
    <div className="uk-offcanvas-content">
      <Header />
      <main>
        <section className="uk-section uk-section-small">
          <div className="uk-container">
            <div className="uk-grid-medium uk-child-width-1-1" uk-grid="">
              <Breadcrumb title={title} hasTitle={hasTitle}/>
              <div>
                <div className="uk-grid-medium" uk-grid="">
                  {children}
                  {hasNavigation && <NavigationSidebar activeTitle={title} />}
                </div>
              </div>
            </div>
          </div>
        </section>
        <Features />
      </main>
      <Footer />
      <MenusSidebar />
      <CartSidebar />
    </div>
  </App>
);

StaticPage.propTypes = {
  title: PropTypes.string.isRequired,
  hasNavigation: PropTypes.bool,
  hasTitle: PropTypes.bool,
  children: PropTypes.object.isRequired
};

export default StaticPage;
