const { store: alert } = require("react-notifications-component");
const { isObject, map, toArray, isArray } = require("lodash");
const notification = {
  type: "danger",
  insert: "top",
  container: "top-right",
  animationIn: ["animated", "fadeIn"],
  animationOut: ["animated", "fadeOut"],
  dismiss: {
    duration: 8000,
    onScreen: true
  }
};

/**
 * Fire error notification alert.
 *
 * @param {object|array|String} error Error variable
 */
const danger = (error, title = "Oops!, Error") => {
  const errors = isObject(error)
    ? toArray(error).reverse()
    : isArray(error)
    ? error
    : [error];
  map(errors, msg =>
    alert.addNotification({
      ...notification,
      title,
      message: msg
    })
  );
};

/**
 * Fire success notification alert.
 *
 * @param {object|array|String} success Success variable
 */
const success = (success, title = "Wow!, Success") => {
  const successes = isObject(success)
    ? toArray(success).reverse()
    : isArray(success)
    ? success
    : [success];
  map(successes, msg =>
    alert.addNotification({
      ...notification,
      type: "success",
      title,
      message: msg
    })
  );
};

/**
 * Fire warning notification alert.
 *
 * @param {object|array|String} warning Warning variable
 */
const warning = (warning, title = "Oops!, Warning") => {
  const warnings = isObject(warning)
    ? toArray(warning).reverse()
    : isArray(warning)
    ? warning
    : [warning];
  map(warnings, msg =>
    alert.addNotification({
      ...notification,
      type: "warning",
      title,
      message: msg
    })
  );
};

/**
 * Fire info notification alert.
 *
 * @param {object|array|String} info Info variable
 */
const info = (info, title = "Notice!, Info") => {
  const infos = isObject(info)
    ? toArray(info).reverse()
    : isArray(info)
    ? info
    : [info];
  map(infos, msg =>
    alert.addNotification({
      ...notification,
      type: "info",
      title,
      message: msg
    })
  );
};

module.exports = { danger, success, warning, info };
