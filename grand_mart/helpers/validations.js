const { map } = require("lodash");
const { validationResult } = require("express-validator");
/**
 * Format validation errors.
 *
 * @param {Array} errors Errors array.
 */

const formatErrors = errors => {
  if (errors instanceof Error) return { error: errors.message };

  let _errors = {};
  map(errors, error => {
    if (_errors[error["param"]]) return;
    _errors[error["param"]] = error.msg;
  });
  return _errors;
};

/**
 * Handle validation errors
 * @param {object} req Request object
 * @param {object} res Response object
 */
const handleErrors = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ error: formatErrors(errors.array()) });
  }
  next();
};

module.exports = { handleErrors, formatErrors };
