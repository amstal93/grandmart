const { Router } = require("express");
const bodyParser = require("body-parser");
const config = require("./../config/config.json");

const router = Router();

// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
router.use(bodyParser.json());

module.exports = router;
