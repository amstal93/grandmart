const { Router } = require("express");
const cors = require("cors");

const router = Router();

router.use(cors());

module.exports = router;
