const { Router } = require("express");
const path = require("path");
const nodemailer = require("nodemailer");
const handlebars = require("express-handlebars");
const hbs = require("nodemailer-express-handlebars");
const config = require("../config/config.json");

const router = Router();

router.use((req, res, next) => {
  const mail = nodemailer.createTransport(config.mail);
  viewEngine = handlebars.create({
    partialsDir: "partials/",
    defaultLayout: false
  });

  const options = {
    viewEngine,
    viewPath: path.resolve(__dirname, "../mails")
  };

  mail.use("compile", hbs(options));

  mail.verify(function(error, success) {
    if (error) {
      console.error(error);
    } else {
      console.log("Mail Server is ready");
    }
  });

  req.mail = mail;
  next();
});

module.exports = router;
