const { Router } = require("express");
const session = require("express-session");
const config = require("../config/config.json");
const uuidv4 = require("uuid/v4");

const router = Router();
const sess = {
  secret: config.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  genid: function(req) {
    return uuidv4(); // use UUIDs for session IDs
  },
  cookie: {}
};

if (process.env.NODE_ENV === "production") {
  router.set("trust proxy", 1); // trust first proxy
  sess.cookie.secure = true; // serve secure cookies
}

router.use(session(sess));

module.exports = router;
