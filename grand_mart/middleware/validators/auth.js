const { Router } = require("express");
const { checkSchema } = require("express-validator");

const router = Router();

router.use(
  "/api/auth/register",
  checkSchema({
    name: {
      isEmpty: {
        negated: true,
        errorMessage: "Name is required"
      }
    },
    email: {
      isEmpty: {
        negated: true,
        errorMessage: "Email is required"
      },
      isEmail: true
    },
    password: {
      isEmpty: {
        negated: true,
        errorMessage: "Password is required"
      },
      isLength: {
        errorMessage: "Password should be at least 6 chars long",
        options: { min: 6 }
      }
    },
    passwordAgain: {
      isEmpty: {
        negated: true,
        errorMessage: "Password Confirmation is required"
      },
      custom: {
        options: (value, { req }) => {
          if (value !== req.body.password) {
            throw new Error("Password confirmation is incorrect");
          }
          return true;
        }
      }
    }
  })
);

module.exports = router;
