const { Router } = require("express");
const auth = require("./auth");
const { handleErrors } = require("../../helpers/validations");

const router = Router();

router.use(auth);
router.use(handleErrors);

module.exports = router;
