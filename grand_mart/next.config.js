const withCSS = require("@zeit/next-css");
const path = require("path");

module.exports = withCSS({
  webpack: config => {
    config.resolve.alias["~"] = __dirname;
    return config;
  }
});
