import { verifyEmailByToken } from "~/helpers/token";

export default (req, res) => {
  if (req.method === "POST") {
    const { hash } = req.body;

    verifyEmailByToken(hash)
      .then(verified =>
        res.json({
          success: verified
        })
      )
      .catch(err => res.status(400).json({ error: err.message }));
  } else {
    return res.status(400).json({ error: "400 Bad Request" });
  }
};

export const config = {
  api: {
    bodyParser: false
  }
};
