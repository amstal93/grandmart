import React, { useState } from "react";
import A from "next/link";
import { connect } from "react-redux";
import StaticPage from "../../components/layouts/StaticPage";
import {
  callLoginApiAction,
  callRegisterApiAction,
  changeLoginFieldAction,
  changeRegisterFieldAction
} from "./../../redux/actions/auth";

const Authentication = props => {
  const {
    login,
    register,
    changeLoginField,
    changeRegisterField,
    callRegisterApi,
    callLoginApi
  } = props;

  const setLoginField = field =>
    changeLoginField(
      field.name,
      field.name === "rememberMe" ? field.checked : field.value
    );

  const setRegisterField = field =>
    changeRegisterField(field.name, field.value);

  const handleRegisterSubmit = e => {
    e.preventDefault();
    callRegisterApi();
  };

  const handleLoginSubmit = e => {
    e.preventDefault();
    callLoginApi();
  };

  return (
    <StaticPage title="Authentication" hasTitle>
      <section className="uk-width-1-1 uk-width-expand@m">
        <article className="uk-card uk-card-default uk-card-small uk-card-body uk-article tm-ignore-container">
          <div
            className="uk-child-width-1-1 uk-child-width-1-2@s uk-margin-top"
            uk-grid=""
          >
            <section>
              <h2 className="uk-text-center">Create an account</h2>
              <form onSubmit={handleRegisterSubmit}>
                <div className="uk-grid-small uk-child-width-1-1" uk-grid="">
                  <div>
                    <label>
                      <div className="uk-form-label uk-form-label-required">
                        Name
                      </div>
                      <input
                        className="uk-input"
                        type="text"
                        name="name"
                        value={register.name}
                        onChange={e => setRegisterField(e.target)}
                      />
                    </label>
                  </div>
                  <div>
                    <label>
                      <div className="uk-form-label uk-form-label-required">
                        Email
                      </div>
                      <input
                        className="uk-input"
                        type="email"
                        name="email"
                        value={register.email}
                        onChange={e => setRegisterField(e.target)}
                      />
                    </label>
                  </div>
                  <div>
                    <label>
                      <div className="uk-form-label uk-form-label-required">
                        Password
                      </div>
                      <input
                        className="uk-input"
                        type="password"
                        name="password"
                        value={register.password}
                        onChange={e => setRegisterField(e.target)}
                      />
                    </label>
                  </div>
                  <div>
                    <label>
                      <div className="uk-form-label uk-form-label-required">
                        Re-type Password
                      </div>
                      <input
                        className="uk-input"
                        type="password"
                        name="passwordAgain"
                        value={register.passwordAgain}
                        onChange={e => setRegisterField(e.target)}
                      />
                    </label>
                  </div>
                  <div className="uk-text-center">
                    <button className="uk-button uk-button-primary">
                      Create my account
                    </button>
                  </div>
                </div>
              </form>
            </section>
            <section>
              <h2 className="uk-text-center">Login</h2>
              <form onSubmit={handleLoginSubmit}>
                <div className="uk-grid-small uk-child-width-1-1" uk-grid="">
                  <div>
                    <label>
                      <div className="uk-form-label uk-form-label-required">
                        Email
                      </div>
                      <input
                        className="uk-input"
                        type="email"
                        name="email"
                        value={login.email}
                        onChange={e => setLoginField(e.target)}
                      />
                    </label>
                  </div>
                  <div>
                    <label>
                      <div className="uk-form-label uk-form-label-required">
                        Password
                      </div>
                      <input
                        className="uk-input"
                        type="password"
                        name="password"
                        value={login.password}
                        onChange={e => setLoginField(e.target)}
                      />
                    </label>
                  </div>
                  <div className="uk-grid-small" uk-grid="">
                    <div className="uk-width-expand">
                      <input
                        className="tm-checkbox"
                        id="remember"
                        type="checkbox"
                        name="rememberMe"
                        checked={login.rememberMe}
                        onChange={e => setLoginField(e.target)}
                      />
                      <label htmlFor="remember">
                        <span>Remember me</span>
                      </label>
                    </div>
                    <div className="uk-text-right">
                      <A href="/auth/reset-password">
                        <a className="uk-link-heading">
                          <span className="tm-pseudo">
                            Forget your password?
                          </span>
                        </a>
                      </A>
                    </div>
                  </div>
                  <div className="uk-text-center">
                    <button className="uk-button uk-button-primary">
                      Login
                    </button>
                  </div>
                </div>
              </form>
            </section>
          </div>
        </article>
      </section>
    </StaticPage>
  );
};

const mapStateToProps = state => ({
  login: state.auth.login,
  register: state.auth.register
});

const mapDispatchToProps = dispatch => ({
  changeLoginField: (field, value) =>
    dispatch(changeLoginFieldAction(field, value)),
  changeRegisterField: (field, value) =>
    dispatch(changeRegisterFieldAction(field, value)),
  callLoginApi: () => dispatch(callLoginApiAction()),
  callRegisterApi: () => dispatch(callRegisterApiAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Authentication);
