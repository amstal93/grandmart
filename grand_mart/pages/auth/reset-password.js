import React from "react";
import StaticPage from "../../components/layouts/StaticPage";

const ResetPassword = () => (
  <StaticPage title="Password Reset" hasTitle>
    <section className="uk-width-1-1 uk-width-expand@m">
      <article className="uk-card uk-card-default uk-card-small uk-card-body uk-article tm-ignore-container">
        <div
          className="uk-child-width-1-1 uk-child-width-1-2@s uk-margin-top"
          uk-grid=""
        >
          <section>
            <h2 className="uk-text-left">Don't panic!</h2>
            <div className="uk-grid-small uk-child-width-1-1" uk-grid="">
              <div>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. In
                  animi perferendis incidunt dolores, obcaecati dolor
                  consequatur non praesentium iste veritatis ea quia, eos,
                  commodi facere! Eius eos necessitatibus repudiandae similique.
                </p>
              </div>
            </div>
          </section>
          <section>
            <h2 className="uk-text-left">Reset my password</h2>
            <form>
              <div className="uk-grid-small uk-child-width-1-1" uk-grid="">
                <div>
                  <label>
                    <div className="uk-form-label uk-form-label-required">
                      Email
                    </div>
                    <input className="uk-input" type="email" required />
                  </label>
                </div>
                <div className="uk-text-center">
                  <button className="uk-button uk-button-primary">
                    Send Password Reset Email
                  </button>
                </div>
              </div>
            </form>
          </section>
        </div>
      </article>
    </section>
  </StaticPage>
);

export default ResetPassword;
