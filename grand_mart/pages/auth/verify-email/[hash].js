import React from "react";
import Api, { API_VERIFY_EMAIL } from "~/redux/api";
import StaticPage from "../../../components/layouts/StaticPage";
function VerifyEmail({ success }) {
  return (
    <StaticPage title="Email Verification" hasTitle>
      <section className="uk-width-1-1 uk-width-expand@m">
        <article className="uk-card uk-card-default uk-card-small uk-card-body uk-article tm-ignore-container">
          <div>
            <header className="site-header" id="header">
              <h1
                className="site-header__title"
                data-lead-id="site-header-title"
              >
                THANK YOU!
              </h1>
            </header>
            <div className="main-content">
              <i
                className="fa fa-check main-content__checkmark"
                uk-icon="check"
                id="checkmark"
              />
              <p
                className="main-content__body"
                data-lead-id="main-content-body"
              >
                Thanks a bunch for filling that out. It means a lot to us, just
                like you do! We really appreciate you giving us a moment of your
                time today. Thanks for being you.
              </p>
            </div>
            <footer className="site-footer" id="footer">
              <p className="site-footer__fineprint" id="fineprint">
                Copyright ©2014 | All Rights Reserved
              </p>
            </footer>
          </div>
        </article>
      </section>
    </StaticPage>
  );
}

VerifyEmail.getInitialProps = async ({ query }) => {
  let data = { success: false };
  try {
    const res = await Api.post(API_VERIFY_EMAIL, {
      hash: query.hash
    });
    data = res.data;
  } catch (error) {}
  return data;
};

export default VerifyEmail;
