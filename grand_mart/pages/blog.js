import React from "react";
import StaticPage from "../components/layouts/StaticPage";

const Blog = () => (
  <StaticPage title="Blog" hasNavigation hasTitle>
    <section className="uk-width-1-1 uk-width-expand@m">
      <div className="uk-grid-medium uk-child-width-1-1" uk-grid="">
        <div>
          <a href="article.html">
            <article className="uk-card uk-card-default uk-card-small uk-article uk-overflow-hidden uk-box-shadow-hover-large uk-height-1-1 tm-ignore-container">
              <div className="tm-ratio tm-ratio-16-9">
                <figure className="tm-media-box uk-cover-container uk-margin-remove">
                  <img
                    src="images/articles/macbook-photo.jpg"
                    alt="Everything You Need to Know About the MacBook Pro"
                    uk-cover="uk-cover"
                  />
                </figure>
              </div>
              <div className="uk-card-body">
                <div className="uk-article-body">
                  <div className="uk-article-meta uk-margin-xsmall-bottom">
                    <time>May 21, 2018</time>
                  </div>
                  <div>
                    <h3 className="uk-margin-remove">
                      Everything You Need to Know About the MacBook Pro
                    </h3>
                  </div>
                  <div className="uk-margin-small-top">
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Proin sodales eget ipsum id aliquam. Nam consectetur
                      interdum nibh eget sodales. Cras volutpat efficitur ornar.
                    </p>
                  </div>
                </div>
              </div>
            </article>
          </a>
        </div>
        <div>
          <a href="article.html">
            <article className="uk-card uk-card-default uk-card-small uk-article uk-overflow-hidden uk-box-shadow-hover-large uk-height-1-1 tm-ignore-container">
              <div className="tm-ratio tm-ratio-16-9">
                <figure className="tm-media-box uk-cover-container uk-margin-remove">
                  <img
                    src="images/articles/macos.jpg"
                    alt="Apple introduces macOS Mojave"
                    uk-cover="uk-cover"
                  />
                </figure>
              </div>
              <div className="uk-card-body">
                <div className="uk-article-body">
                  <div className="uk-article-meta uk-margin-xsmall-bottom">
                    <time>May 21, 2018</time>
                  </div>
                  <div>
                    <h3 className="uk-margin-remove">
                      Apple introduces macOS Mojave
                    </h3>
                  </div>
                  <div className="uk-margin-small-top">
                    <p>
                      Praesent consequat justo eu massa malesuada posuere. Donec
                      ultricies tincidunt nisl, sed euismod nulla venenatis
                      maximus. Maecenas sit amet semper tellus. Pellentesque
                      imperdiet finibus sapien, a consectetur eros auctor a.
                    </p>
                  </div>
                </div>
              </div>
            </article>
          </a>
        </div>
      </div>
    </section>
  </StaticPage>
);

export default Blog;
