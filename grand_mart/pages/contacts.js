import React from "react";
import StaticPage from "../components/layouts/StaticPage";

const Contacts = () => (
  <StaticPage title="Contacts" hasNavigation hasTitle>
    <section className="uk-width-1-1 uk-width-expand@m">
      <article className="uk-card uk-card-default uk-card-small uk-card-body uk-article tm-ignore-container">
        <div className="tm-wrapper">
          <figure
            className="tm-ratio tm-ratio-16-9 js-map"
            data-latitude="59.9356728"
            data-longitude="30.3258604"
            data-zoom={14}
          />
        </div>
        <div
          className="uk-child-width-1-1 uk-child-width-1-2@s uk-margin-top"
          uk-grid=""
        >
          <section>
            <h3>Store</h3>
            <ul className="uk-list">
              <li>
                <a className="uk-link-heading" href="#">
                  <span className="uk-margin-small-right" uk-icon="receiver" />
                  <span className="tm-pseudo">8 800 799 99 99</span>
                </a>
              </li>
              <li>
                <a className="uk-link-heading" href="#">
                  <span className="uk-margin-small-right" uk-icon="mail" />
                  <span className="tm-pseudo">example@example.com</span>
                </a>
              </li>
              <li>
                <div>
                  <span className="uk-margin-small-right" uk-icon="location" />
                  <span>St.&nbsp;Petersburg, Nevsky&nbsp;Prospect&nbsp;28</span>
                </div>
              </li>
              <li>
                <div>
                  <span className="uk-margin-small-right" uk-icon="clock" />
                  <span>Daily 10:00–22:00</span>
                </div>
              </li>
            </ul>
          </section>
          <section>
            <h3>Feedback</h3>
            <dl className="uk-description-list">
              <dt>Cooperation</dt>
              <dd>
                <a className="uk-link-muted" href="#">
                  cooperation@example.com
                </a>
              </dd>
              <dt>Partners</dt>
              <dd>
                <a className="uk-link-muted" href="#">
                  partners@example.com
                </a>
              </dd>
              <dt>Press</dt>
              <dd>
                <a className="uk-link-muted" href="#">
                  press@example.com
                </a>
              </dd>
            </dl>
          </section>
          <section className="uk-width-1-1">
            <h2 className="uk-text-center">Contact Us</h2>
            <form>
              <div className="uk-grid-small uk-child-width-1-1" uk-grid="">
                <div>
                  <label>
                    <div className="uk-form-label uk-form-label-required">
                      Name
                    </div>
                    <input className="uk-input" type="text" required />
                  </label>
                </div>
                <div>
                  <label>
                    <div className="uk-form-label uk-form-label-required">
                      Email
                    </div>
                    <input className="uk-input" type="email" required />
                  </label>
                </div>
                <div>
                  <label>
                    <div className="uk-form-label">Topic</div>
                    <select className="uk-select">
                      <option>Customer service</option>
                      <option>Tech support</option>
                      <option>Other</option>
                    </select>
                  </label>
                </div>
                <div>
                  <label>
                    <div className="uk-form-label">Message</div>
                    <textarea
                      className="uk-textarea"
                      rows={5}
                      defaultValue={""}
                    />
                  </label>
                </div>
                <div className="uk-text-center">
                  <button className="uk-button uk-button-primary">Send</button>
                </div>
              </div>
            </form>
          </section>
        </div>
      </article>
    </section>
  </StaticPage>
);

export default Contacts;
