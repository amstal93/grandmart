import {
  CHANGE_LOGIN_FIELD,
  CHANGE_REGISTER_FIELD,
  API_REGISTER_REQUEST,
  API_LOGIN_REQUEST
} from "./../constants";

export const changeLoginFieldAction = (field, value) => ({
  type: CHANGE_LOGIN_FIELD,
  payload: { [field]: value }
});

export const changeRegisterFieldAction = (field, value) => ({
  type: CHANGE_REGISTER_FIELD,
  payload: { [field]: value }
});

export const callRegisterApiAction = () => ({
  type: API_REGISTER_REQUEST
});

export const callLoginApiAction = () => ({
  type: API_LOGIN_REQUEST
});
