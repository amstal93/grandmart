import { call, select, takeLatest, all } from "redux-saga/effects";
import { danger, success } from "~/helpers/alert";
import Api, { API_REGISTER, API_LOGIN } from "../api";
import { API_REGISTER_REQUEST, API_LOGIN_REQUEST } from "../constants";

/**
 * Worker Saga for Login
 */
function* workerLogin() {
  try {
    const { data } = yield call(
      Api.post,
      API_LOGIN,
      yield select(state => state.auth.login)
    );

    success(data);
  } catch ({
    response: {
      data: { error }
    }
  }) {
    error
      ? danger(error)
      : danger("Unexpected Error occurs. Please try again later.");
  }
}
/**
 * Worker Saga for Register
 */
function* workerRegister() {
  try {
    const { data } = yield call(
      Api.post,
      API_REGISTER,
      yield select(state => state.auth.register)
    );

    success(data);
  } catch ({
    response: {
      data: { error }
    }
  }) {
    danger(error);
  }
}

/**
 * Watcher Saga
 */
export default function*() {
  yield all([
    takeLatest(API_REGISTER_REQUEST, workerRegister),
    takeLatest(API_LOGIN_REQUEST, workerLogin)
  ]);
}
